package nova.assignment.suraj.model;

import lombok.Data;

@Data
public class User {
    int id;
    String email;
    String password;
    String name;
    int age;
}
