package nova.assignment.suraj.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity(name = "order_table")
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    int id;
    int product_id;
    int quantity;
    String date;
    String status;

    public void setproduct_id(int product_id) {
        this.product_id=product_id;
    }
}
