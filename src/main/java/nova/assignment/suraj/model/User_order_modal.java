package nova.assignment.suraj.model;

import lombok.Data;

@Data
public class User_order_modal {
    int id;
    int user_id;
    int product_id;
}
