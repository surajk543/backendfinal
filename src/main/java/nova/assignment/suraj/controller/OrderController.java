package nova.assignment.suraj.controller;

import nova.assignment.suraj.model.Order;
import nova.assignment.suraj.model.Product;
import nova.assignment.suraj.repository.OrderRepository;
import nova.assignment.suraj.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;
@RestController
public class OrderController {

    @Autowired ProductRepository productRepository;
    @Autowired OrderRepository orderRepository;
    @PostMapping("/order/{product_id}")
    public String insert(@PathVariable int product_id, @RequestBody Order order){
        Optional<Product> optionalProduct= productRepository.findById(product_id);
        if(optionalProduct.isPresent()){
            order.setproduct_id(product_id);
            orderRepository.save(order);
            return "Succesfull";
        }
        return "Product not Present";
    }
}
