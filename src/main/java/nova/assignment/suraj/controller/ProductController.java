package nova.assignment.suraj.controller;

import nova.assignment.suraj.dao.ProductDao;
import nova.assignment.suraj.model.Product;
import nova.assignment.suraj.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ProductController {
  @Autowired
  private ProductRepository productRepository;
  @GetMapping("/product")
  public List<Product> get() {
    return productRepository.findAll();
  }

  @PostMapping("/product")
  public Product insert(@RequestBody Product product) {
    return productRepository.save(product);
  }
}
