package nova.assignment.suraj.controller;

import nova.assignment.suraj.dao.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {
  @Autowired
  UserDao userDao;

  @PostMapping("/user")
  public void insert() {
    userDao.insertrecord();
  }
}
