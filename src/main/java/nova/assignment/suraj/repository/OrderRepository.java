package nova.assignment.suraj.repository;

import nova.assignment.suraj.model.Order;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderRepository extends JpaRepository<Order,Integer> {

}
