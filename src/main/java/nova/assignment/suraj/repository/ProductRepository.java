package nova.assignment.suraj.repository;

import nova.assignment.suraj.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product,Integer> {

}
